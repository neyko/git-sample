const express = require('express')
var mysql = require('mysql');
var config = require('./config');

const app = express()
const port = 3000

var con = mysql.createConnection(config);
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

app.use(express.json())
app.use(express.static('static'))

app.get('/customer', (req, res) => {
    
    if (req.param('id')) {
	    con.query("SELECT * FROM customer WHERE ID=?", [req.param('id')], function (err, result) {
	       if (err) throw err;
	       if (result.length > 0) {
		       console.log("Result: " + [result[0].Name, result[0].Surname].join(' '));
	           res.send(result[0])
	       } else {
			   res.send('err: no result!')
	       }
	    });
    } else {
		res.send('please specify customer id! ')
	}
})

app.post("/customer", (req, resp) => {
	console.log(req.body)
	con.query("INSERT INTO customer (Name, Surname, Regplate) VALUES (?,?,?)", [req.body.name, req.body.surname, req.body.lpn], function (err, result) {
       if (err) {
          resp.send(err);
       } else {
	       resp.send("ok")
       }
    })
})


app.delete("/customer", (req, resp) => {
	console.log('request for deleting customer id=' + req.query.id)
	con.query("DELETE FROM customer WHERE ID=?", [req.query.id], (err, result) => {
		if (err) {
          resp.send(err);
       } else {
	       resp.send("ok")
       }
	})
})


app.put("/customer", (req, resp) => {
	con.query("UPDATE customer SET Name=?,Surname=?,Regplate=? WHERE ID=?", [req.body.name, req.body.surname, req.body.lpn, req.body.id], (err, result) => {
       if (err) {
          resp.send(err);
       } else {
	       resp.send("ok")
       }
	})
})

app.get('/customers-last5', (req, res) => {
    
    con.query("SELECT * FROM customer ORDER BY id DESC LIMIT 5", function (err, result) {
       if (err) throw err;
       res.send(result)
    });
})

app.get('/customers', (req, res) => {
    
    con.query("SELECT * FROM customer", function (err, result) {
       if (err) throw err;
       res.send(result)
    });
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})